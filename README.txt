Overview
Simple functionality using page url patterns to set a different logos on the drupal pages.
Wildcards can be used.
Supports multiple pages as well as overriding of the pages for the logos.

Usage
You can have only one logo for the same page with the same pattern for example node/5. But you can have many with wildcards like "" or node/*. The more specific match is being used only, so node/5 will override node/*.

Specifics
Creating a logo with the same page as another logo will erase the record from the other logo but will not delete it.
Already used logos and pages are being listed on every logo edit, create page as well as on the administration page located at admin/content/extra_logos/

For Developers
The module overrides $variables['logo'] in HOOK_preprocess_page().
New directory is being created in the files directory for the logos to be stored separately.Extra Logos
