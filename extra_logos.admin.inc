<?php

/**
 * @file
 * Administration page callbacks for the Extra Logos module.
 */

/**
 * Gets a list of all variants of a page
 *
 * @param string $task_key
 * @param string $prefix_name
 * @return
 *   Array of all variants of a page
 */
function _extra_logos_get_page_variants($task_key, $prefix_name = '') {
  $variants = array();
  $cache = page_manager_get_page_cache($task_key);
  foreach ($cache->handlers as $handler) {
    $cache_key = "{$handler->handler}:{$task_key}:{$handler->name}";
    $variants[$cache_key] = $prefix_name . $handler->conf['title'];
  }
  return $variants;
}

/**
 * Gets a list of options (for the checkboxes) with all possible pages, including all page variants
 *
 * @return
 *   Array of all pages and page variants
 */
function _extra_logos_get_pages_options() {
  $pages = array();

  // If there is no page manager.
  if (!function_exists('page_manager_get_tasks_by_type')) {
    return $pages;
  }

  ctools_include('context');
  $tasks = page_manager_get_tasks_by_type('page');

  foreach ($tasks as $task) {
    $subtasks = page_manager_get_task_subtasks($task);
    if ($subtasks) {
      foreach ($subtasks as $subtask) {
        $task_key = $task['name'] . '-' . $subtask['name'];
        $pages += _extra_logos_get_page_variants($task_key, "$task_key: ");
      }
    }
    else {
      $task_key = $task['name'];
      $pages += _extra_logos_get_page_variants($task_key, "$task_key: ");
    }
  }
  // do we want to hide some pages????

  return $pages;
}

/**
 * Shows the currently seelcted logo for a page, if it is different from the logo we're currently setting
 *
 * @param int $lid
 *   The id of the logo we're currently editing
 *
 * @return
 *   Array of pages options
 */
function _extra_logos_pages_options_show_current($lid = 0) {
  // Show current logo if already selected for a page.
  $rs = db_query('SELECT * FROM {extra_logos_page} WHERE lid <> :lid', array(':lid' => $lid));
  $pages = array();

  foreach ($rs as $result) {
    $page = $result->page;
    $logo = db_query('SELECT * FROM {extra_logos} WHERE lid = :lid', array(':lid' => $result->lid))->fetchAssoc();

    if (!empty($logo['fid']) && $file = file_load($logo['fid'])) {
      $logo_image = theme('image', array('path' => $file->uri, 'height' => 40));

      // Group by logo id.
      if (!empty($pages[$result->lid])) {
        $pages[$result->lid]['pages'][] = $page;
      }
      else {
        $pages[$result->lid] = array(
          'pages' => array($page),
          'name' => $logo['name'],
          'image' => $logo_image,
        );
      }
    }
  }

  return $pages;
}

/**
 * Outputs a table with all logos and their settings
 *
 * @return string
 *   HTML formatted table
 */
function extra_logos_admin_list() {
  $output = '';

  $rs = db_select('extra_logos', 'c')
    ->fields('c')
    ->orderBy('name');
  $rs_result = $rs->execute();

  $logolist = array();

  while($logo = $rs_result->fetchObject()) {
    $edit = l(t('edit'), '/admin/content/extra_logos/edit/' . $logo->lid);
    $delete = l(t('delete'), '/admin/content/extra_logos/delete/' . $logo->lid);

    $pages = "";

    $pages_query = db_select('extra_logos_page', 'p')
      ->fields('p')
      ->condition('lid', $logo->lid);
    $pages_query_result = $pages_query->execute();

    while ($page = $pages_query_result->fetchObject()) {
      if (!empty($page->page)) {
        $pages .= htmlspecialchars($page->page) . "<br />";
      }
    }
    if ($logo->fid && $file = file_load($logo->fid)) {
      $image = theme('image', array('path' => $file->uri, 'height' => 30));
    }
    else {
      $image = t('Missing image');
    }

    $logolist[] = array(
      $logo->name,
      $image,
      $pages,
      $edit . ' ' . $delete,
    );

  }
  $header = array(t('Name'), t('Image'), t('Current pages'), t('Actions'));
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $logolist,
  ));
  return $output;
}

/**
 * Form builder. Configure logos.
 * Handles 'admin/content/extra_logos/add' and 'admin/content/extra_logos/edit/%' in extra_logos_menu.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function extra_logos_admin_settings($form, &$form_state, $logoid = 0) {
  if ($logoid != 0) {
    $logo = db_select('extra_logos', 'c')
      ->fields('c')
      ->condition('lid', $logoid)
      ->execute()
      ->fetchAssoc();
  }

  $form['lid'] = array(
    '#type' => 'value',
    '#value' => $logoid,
  );

  $form['logo_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo name'),
    '#size' => 20,
    '#description' => t('Enter the logo name.'),
    '#default_value' => (isset($logo) ? $logo['name'] : ""),
    '#required' => TRUE,
  );

  $current_logo = !empty($logo) ? file_load($logo['fid']) : FALSE;

  if (!empty($current_logo->uri)) {
    $form['current_logo'] = array(
      '#markup' => (isset($logo['fid']) && $logo['fid']) ? ('<div> ' . theme('image', array(
          'path' => $current_logo->uri,
          'height' => 100,
        )) . ' </div>') : "",
    );
  }

  $form['logo_file'] = array(
    '#type' => 'managed_file',
    '#title' => 'Change logo file',
    '#field_name' => "Extra Logos",
    '#description' => t('Upload a new logo file. The recommended dimensions of the image are 30 x 100px.'),
    '#required' => empty($logo['fid']),
    '#upload_location' => 'public://' . variable_get('extra_logos_dir', 'extra_logos') . '/',
  );

  $pagelist = "";
  if (isset($logo)) {
    $rs = db_select('extra_logos_page', 'p')
      ->fields('p')
      ->condition('lid', $logo['lid']);
    $rs_result = $rs->execute();

    while($page = $rs_result->fetchAssoc()) {
      $pagelist .= $page['page'] . PHP_EOL;
    }
  }

  $already_selected = _extra_logos_pages_options_show_current($logoid);
  $used_logos_rows = array();

  foreach ($already_selected as $item) {
    $used_logos_rows[] = array(
      htmlspecialchars(implode(' ', $item['pages'])),
      $item['name'],
      $item['image'],
    );
  }

  $used_logos_header = array(t('Page'), t('Name'), t('Image'));
  $used_logos_table = theme('table', array(
    'header' => $used_logos_header,
    'rows' => $used_logos_rows,
  ));

  $form['extra_logos_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Use this logo for the following pages:'),
    '#default_value' => $pagelist,
    '#description' => t('Select which pages to use the Extra Logos.
        If no Extra Logo is selected for a page, default logo will be shown.
        Selecting a new Extra Logos for a page which already uses another Extra Logos
        will unset the previous one. <br/> Specify pages by using their paths. Enter one path per line. The ‘*’ character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.', array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    )),
    '#suffix' => "Already used logos and pages:<br />" . $used_logos_table,
  );

  $form['#attributes']['enctype'] = 'multipart/form-data';

  // Use system settings form theme and unset elements which are not needed
  $form = system_settings_form($form);
  unset($form['#submit']);
  unset($form['buttons']['reset']);

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function extra_logos_admin_settings_validate($form, &$form_state) {
  $directory = 'public://' . variable_get('extra_logos_dir', 'extra_logos') . '/';
  if (!is_dir($directory) && !mkdir($directory, 0777)) {
    form_set_error('logo_file', t('Unable to create the Extra Logo directory.'));
    return;
  }
}

/**
 * Implements hook_form_submit().
 */
function extra_logos_admin_settings_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/content/extra_logos';
  $lid = $form_state['values']['lid'];
  $pages = trim($form_state['values']['extra_logos_pages']);
  $pages = preg_split("/[\s]+/", $pages);
  $file = (isset($form_state['values']['logo_file'])) ? file_load($form_state['values']['logo_file']) : '';
  $record = array(
    'name' => $form_state['values']['logo_name'],
  );

  if ($file) {
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'extra_logos', 'extra_logos_image', $lid);

    // Copy the file from temp to our directory
    // We do not add a record in the file table to keep the file inaccessible from other file browsers

    if (file_exists($file->uri)) {
      $record['fid'] = $file->fid;
      extra_logos_delete_file($lid); // Remove old logo image
    }
    else {
      drupal_set_message(t('Failed to upload file. Logo not saved.'), 'error');
      return;
    }
  }

  if ($lid) {
    // We edit an existing logo.
    $record['lid'] = $lid;
    // Remove old file usage.
    $logo = db_select('extra_logos', 'c')
      ->fields('c')
      ->condition('lid', $lid)
      ->execute()
      ->fetchAssoc();

    if ($logo['fid'] && $file = file_load($logo['fid'])) {
      file_usage_delete($file, 'extra_logos', 'extra_logos_image', $lid);

      // Remove old file.
      if (!file_usage_list($file)) {
        extra_logos_delete_file($lid);
      }
    }
    // DELETE and INSERT pages instead of check for UPDATE
    db_delete('extra_logos_page')
      ->condition('lid', $lid)
      ->execute();
  }

  if (!$lid) {
    $lid = db_insert('extra_logos')->fields($record)->execute();
  }
  else {
    db_update('extra_logos')
      ->fields($record)
      ->condition('lid', $lid)
      ->execute();
  }

  foreach ($pages as $page) {
    db_query("REPLACE INTO {extra_logos_page} (page, lid)
      VALUES ('$page', :lid)", array(':lid' => $lid));
  }
}

/**
 * Handles 'admin/content/extra_logos/delete/%' in extra_logos_menu.
 */
function extra_logos_delete($form, &$form_state, $logoid = NULL) {
  if ($logoid != NULL) {
    $logo = db_query('SELECT * FROM {extra_logos} WHERE lid = :lid', array(':lid' => $logoid))->fetchObject();
    if ($logo) {
      $form = array();
      $form['lid'] = array(
        '#type' => 'value',
        '#value' => $logoid,
      );
      $form['img'] = array(
        '#markup' => theme('image', array(
          'path' => file_load($logo->fid)->uri,
          'height' => 100,
        )),
      );
      $question = t('Are you sure you want to delete this Extra Logo?');
      $path = 'admin/content/extra_logos';
      return confirm_form($form, $question, $path);
    }
  }
  drupal_not_found();
  exit;
}

/**
 * Implements hook_form_submit().
 */
function extra_logos_delete_submit($form, &$form_state) {
  $lid = $form_state['values']['lid'];
  extra_logos_delete_file($lid);

  db_delete('extra_logos')
    ->condition('lid', $lid)
    ->execute();

  db_delete('extra_logos_page')
    ->condition('lid', $lid)
    ->execute();

  $form_state['redirect'] = 'admin/content/extra_logos';
}

/**
 * Deletes a file by logo id
 */
function extra_logos_delete_file($lid) {
  if ($lid) {
    // Find and delete the file.
    $fid = db_select('extra_logos', 'c')
      ->fields('c', array('fid'))
      ->condition('lid', $lid)
      ->execute()
      ->fetchAssoc();

    if (!empty($fid['fid']) && $file = file_load($fid['fid'])) {
      file_delete($file);
    }
  }
}
